///////////////////////////////////////////////////////////////////////////////
//	Author:		Jacek 'J_SEC' Szydlowski									 //
//	Date:		04.09.2017													 //
//	Brief:		Short introduction to Go syntax and structure				 //
///////////////////////////////////////////////////////////////////////////////

// First statement in every Go file
// Go runtime always executes function main in package main on execution
// of any program
package main

// Import block
// Go dont allow importing not used packages
import (
	"fmt"
	//	"strings" // Compilation error if package not used
)

// Function syntax
func main() {
	fmt.Println("Hello, World!")
	fmt.Println("Increment =", increment(42))

	var n1 int = 0
	n2 := 0
	n1, n2 = neighbour(42)
	fmt.Println("Neighbours =", n1, ",", n2)
}

// Note on semicolons
//func main();
//{
//	fmt.Println("Hello, World!");
//}

func increment(value int) int {
	return value + 1
}

// Go function can return multiple values
func neighbour(value int) (int, int) {
	return value - 1, value + 1
}
